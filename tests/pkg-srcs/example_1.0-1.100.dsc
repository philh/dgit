Format: 3.0 (quilt)
Source: example
Binary: example
Architecture: all
Version: 1.0-1.100
Maintainer: Ian Jackson <ijackson@chiark.greenend.org.uk>
Standards-Version: 3.9.4.0
Build-Depends: debhelper (>= 8)
Package-List:
 example deb devel extra arch=all
Checksums-Sha1:
 2bc730f941db49de57e9678fb0b07bd95507bb44 236 example_1.0.orig-docs.tar.gz
 4bff9170ce9b10cb59937195c5ae2c73719fe150 373 example_1.0.orig.tar.gz
 94dbd51291d7534a9e50a2cebfb1f484999e877b 2156 example_1.0-1.100.debian.tar.xz
Checksums-Sha256:
 ad9671f6b25cdd9f0573f803f702448a45a45183db1d79701aa760bccbeed29c 236 example_1.0.orig-docs.tar.gz
 a3ef7c951152f3ec754f96fd483457aa88ba06df3084e6f1cc7c25b669567c17 373 example_1.0.orig.tar.gz
 1d76c05cc52b1bc517defb336b7d1e8e59651fc19cd6e303b88be45558adca08 2156 example_1.0-1.100.debian.tar.xz
Files:
 cb0cb5487b1e5bcb82547396b4fe93e5 236 example_1.0.orig-docs.tar.gz
 599f47808a7754c66aea3cda1b3208d6 373 example_1.0.orig.tar.gz
 7f30fd213f0e30613615b2c196c48635 2156 example_1.0-1.100.debian.tar.xz
